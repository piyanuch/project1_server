const sql = require("mysql"); // import lib _ sql
const database = sql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "test1", // ที่อยู่ , username, password, ชื่อ database
});

emailCheckRepeat = (req, res, next) => {
  const { firstname, lastname, email, password } = req.body; // recheck ข้อมูล
  database.query(
    "SELECT email FROM userdata WHERE email = ?",
    [email], // recheck email ซ้ำไหมกับ table data => [email] from form
    async (error, result) => {
      // check error ?
      if (error) {
        console.log(error);
      }
      if (result.length > 0) {
        res.status(400).send({ message: "this email is already used" });
        return; // ถ้าซ้ำให้ส่งกลับ
      }
      next();
    }
  );
};
const registerCheck = { emailCheckRepeat: emailCheckRepeat }; // json
module.exports = registerCheck;
