const registerCheck = require("../middleware/registerCheck");
const controller = require("../controllers/authcontroller");

module.exports = function (app) {
  app.use((req, res, next) => {
    res.header(
      // ตั้งค่ารับส่งข้อมูล header of http
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });
  app.post(
    // จัด route ของข้อมูลที่ได้รับมา
    "/api/auth/register",
    [registerCheck.emailCheckRepeat],
    controller.register
  );
  app.post("/api/auth/login", controller.login);
};
