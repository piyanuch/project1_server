const mysql = require("mysql");
const bcrypt = require("bcrypt");

const mysqldb = mysql.createConnection({
  host: process.env.DATABASE_HOST,
  user: process.env.DATABASE_USER,
  password: process.env.DATABASE_PASSWORD,
  database: process.env.DATABASE,
});

exports.register = (req, res) => {
  console.log(req.body);

  const { name, email, password, passwordConfirm } = req.body;
  mysqldb.query(
    "SELECT email FROM users WHERE email = ?",
    [email],
    async (error, results) => {
      if (error) {
        console.log(error);
      }
      if (results.length > 0) {
        return res.render("register", {
          message: "This email is already in use",
        });
      } else if (password !== passwordConfirm) {
        console.log("Passwords do not match");
        return res.render("register", {
          message: "Passwords do not match",
        });
      }
      let hashPass = await bcrypt.hash(password, 8);
      console.log(hashPass);
      mysqldb.query(
        "INSERT INTO users SET?",
        {
          name: name,
          email: email,
          password: hashPass,
        },
        (error, results) => {
          if (error) {
            console.log(error);
          } else {
            res.render("register", { message: "User registered" });
          }
        }
      );
    }
  );
};

exports.login = (req, res) => {
  console.log(req.body);

  const { email, password } = req.body;
  mysqldb.query(
    "SELECT * FROM users WHERE email = ?",
    [email],
    async (error, results) => {
      if (error) {
        console.log(error);
      }
      if (results.length == 0) {
        return res.render("login", {
          message: "Invalid Email or Password",
        });
      } else if (password.length == 0) {
        return res.render("login", {
          message: "Password is empty",
        });
      }

      bcrypt.compare(password, results[0].password).then((compare_result) => {
        console.log(compare_result);
        if (compare_result) {
          req.session.isLoggedIn = true;
          req.session.userID = results[0].id;
          return res.redirect("/");
        } else {
          return res.render("login", {
            message: "Invalid Email or Password",
          });
        }
      });
    }
  );
};
