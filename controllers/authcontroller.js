const mysql = require("mysql");
const bcrypt = require("bcrypt");
const jsonwebtoken = require("jsonwebtoken");

const mysqldb = mysql.createConnection({
  host: process.env.DATABASE_HOST,
  user: process.env.DATABASE_USER,
  password: process.env.DATABASE_PASSWORD,
  database: process.env.DATABASE,
});

exports.register = (req, res) => {
  const { firstname, lastname, email, password, displayname } = req.body;
  //console.log(req.body);
  mysqldb.query(
    // check email
    "SELECT email FROM userdata WHERE email = ?",
    [email],
    async (error, results) => {
      if (error) {
        // error ของการดึงข้อมูล
        console.log(error);
      }
      if (results.length > 0) {
        return res.status(400).send({ message: "this email is already used" });
      }
      var bcryptpassword = await bcrypt.hash(password, 10); // เข้ารหัส password
      mysqldb.query(
        "INSERT INTO userdata SET?",
        {
          firstname: firstname,
          lastname: lastname,
          email: email,
          displayname: displayname,
          password: bcryptpassword, // ข้างหน้ามาจาก database, หลังคือข้อมูล
        },
        (error, results) => {
          if (error) {
            console.log(error);
          } else {
            res.send({ message: "REGISTER SUCCESS" });
          }
        }
      );
    }
  );
};

exports.login = (req, res) => {
  const { email, password } = req.body;
  console.log("email: " + email);
  console.log("pass: " + password);
  mysqldb.query(
    // check email
    "SELECT * FROM userdata WHERE email = ?", // * คือใช้ email เป็นตัวหลัก ในการหาข้อมูล
    [email],
    async (error, results) => {
      if (error) {
        console.log(error);
      }
      if (results.length == 0) {
        // email ที่ login ไม่มี
        return res.status(404).send({ message: "NOT FOUND THIS EMAIL" });
      }
      var passwordcompare = await bcrypt.compare(password, results[0].password); // compare between password user / password data
      if (passwordcompare == false) {
        return res
          .status(401)
          .send({ message: "Incorrect Password", accessToken: null }); // reject กลับ โดยไม่ใช่ token กับ user ที่ login ผิดเข้ามา
      }
      var token = jsonwebtoken.sign({ id: results[0].id }, "my-secret-key", {
        expiresIn: 14400,
      }); //assign token ให้ id
      console.log("token: ");
      console.log(token);
      res // ส่ง token ให้ user
        .status(200)
        .send({
          id: results[0].id,
          email: results[0].email,
          displayname: results[0].displayname,
          accessToken: token,
        });
    }
  );
};
