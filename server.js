const path = require("path");
const express = require("express");
const mysql = require("mysql");
const http = require("http");
const cookie = require("cookie-session");
const dotenv = require("dotenv");
const app = express();
const server = http.createServer(app);
const cors = require("cors");

const io = require("socket.io")(server, {
  cors: {
    origin: "*", // ประกาศให้ io ไปเกาะ server ให้ server อื่น connect ได้
  },
});

const corsOption = {
  origin: "*",
};

app.use(cors(corsOption));

dotenv.config({ path: "./.env" }); // เรียกค่าใช้งาน

const database = mysql.createConnection({
  host: process.env.DATABASE_HOST,
  user: process.env.DATABASE_USER,
  password: process.env.DATABASE_PASSWORD,
  database: process.env.DATABASE,
});

const publicDirectory = path.join(__dirname, "./public"); //dirname หาทุก directory

app.use(express.static(publicDirectory)); // เรียกใช้ public directory (เช่น css)
app.use(express.urlencoded({ extended: true }));
app.use(express.json()); // express ส่งข้อมูลแบบ json

app.use(
  cookie({
    name: "session",
    keys: ["key1", "key2"], // pattern
    maxAge: 1000 * 60 * 60 * 4, // อยู่ได้ 4 hr
  })
);

database.connect((error) => {
  // Connect Database
  if (error) {
    console.log(error);
  } else {
    console.log("DATABASE OK");
  }
});

require("./routes/authroute")(app);

//////////////////////////////
var dataonlineuser = []; // ***เก็บ data ไว้ที่นี้
/////////////////////////////
io.on("connection", (socket) => {
  console.log("userConnected");
  console.log(socket.id); // socket ได้รับหรือยัง
  socket.emit("testConnect", "Socket OK"); // ส่ง packet หา client / testConnect_ชื่อ packet // ข้อมูลที่ส่ง
  socket.on("testSendUser", (data) => {
    //console.log(data);
    var temp_data = {
      // เก็บข้อมูลที่่ server
      id: data.id,
      email: data.email,
      displayname: data.displayname,
    };
    //console.log("tempdata"); // คั่น
    //console.log(temp_data);

    var userindex = dataonlineuser.findIndex((temp) => {
      return temp.email == temp_data.email;
    });
    // show useronline
    //console.log("index " + userindex);
    if (userindex === -1) {
      // ถ้า index ไม่ซ้ำ
      dataonlineuser.push(temp_data);
    }
    console.log(dataonlineuser); // test onlineuser
    io.sockets.emit("dataonlineuser", dataonlineuser); // io.sockets.emit ส่งไปทุก client
  });
  let previousId; // leave room ก่อน
  const safeJoin = (currentId) => {
    console.log("safejoin");
    socket.leave(previousId);
    socket.join(currentId);
    previousId = currentId;
  };
  socket.on("joinRoom", (roomid) => {
    // keep joinroom
    safeJoin(roomid);
  });

  socket.on("sendMsg", (data) => {
    // function รับและส่ง
    io.sockets.to(data.rid).emit("sendMsg", data);
  });

  socket.on("userdisconnect", (userDisconnect) => {
    // dataonlineuser เป็นตัวตั้ง, userDisconnect เป็นตัวที่มาเทียบ
    var userofDisconnect = dataonlineuser.findIndex((obj) => {
      return obj.email == userDisconnect;
    });
    if (userofDisconnect !== -1) {
      dataonlineuser.splice(userofDisconnect, 1);
    }
    io.sockets.emit("dataonlineuser", dataonlineuser);
  });
});

server.listen(4500, () => {
  // start server on port 4500
  console.log("Server OK");
});
